
document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('btn').addEventListener('click', function() {
        let results={}
        let func = document.getElementById('function').value;
        func = func.replace(/\s/g, '');
        func = func.toLowerCase();
        var result = nerdamer('diff(' + func + ')').evaluate();
        var result2 = nerdamer('diff(' + result.text() + ')').evaluate();
        results.f1 = result.text().replace(/\*/g, ''); 
        results.f2 = result2.text().replace(/\*/g, '');           
        document.getElementById('result').innerHTML = 'f\'(x) = ' + results.f1 + (results.f2  != 0 ?'<br>f\'\'(x) = ' + results.f2 : '');
    });
});




